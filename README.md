D3-powered force-directed graph shows the most popular superhero fights on the internet. The data collected from various comics forum, mostly "top fights" lists. Includes two mainstream publisher, Marvel and DC.

**Superhero Alliance Network**: http://seyit.gitlab.io/superhero-alliance-network/
